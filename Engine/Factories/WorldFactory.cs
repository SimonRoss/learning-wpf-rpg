﻿using System;
using System.Collections.Generic;
using System.Text;
using Engine.Models;

namespace Engine.Factories
{
    internal class WorldFactory
    {
        internal World CreateWorld()
        {
            World newWorld = new World();
            newWorld.AddLocation(0,-1,"Home", "This is your home", "/Engine;component/Images/Locations/Home.png");
            newWorld.AddLocation(-1, -1, "Farmer's House", "This is a large farm house owned by Ted the Farmer'", "/Engine;component/Images/Locations/Home.png");
            newWorld.AddLocation(-2, -1, "Farmer's Field", "There are rows of crops growing here, and giant rats hiding between them", "/Engine;component/Images/Locations/Home.png");
            newWorld.AddLocation(0, 0, "Town Square", "The center of town. Rent is sky high and the only enemies are the local tax collectors.", "/Engine;component/Images/Locations/Home.png");
            newWorld.AddLocation(0, 1, "Herbalist's Hut'", "A crazed old lady lives here. She runs a mommy blog and a prominent local anti-vaccine group", "/Engine;component/Images/Locations/Home.png");
            newWorld.AddLocation(0, 2, "Herb Garden", "There are to many damn snakes in this garden!", "/Engine;component/Images/Locations/Home.png");
            newWorld.AddLocation(-1, 0, "Trading Shop", "This is a store", "/Engine;component/Images/Locations/Home.png");
            newWorld.AddLocation(1, 0, "Town Gate", "Its a large sturdy gate. You've always worried it might fall on you", "/Engine;component/Images/Locations/Home.png");
            newWorld.AddLocation(2, 0, "Spider Forest", "Judging by the name, you wonder why anyone would ever go here", "/Engine;component/Images/Locations/Home.png");

            return newWorld;
        }
    }
}
