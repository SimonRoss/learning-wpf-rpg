﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Engine.Models
{
    public class World
    {
        private List<Location> _locations = new List<Location>();

        internal void AddLocation(int xCoordinate, int yCoordinate, string name , string description, string ImageName)
        {
            Location loc = new Location();
            loc.Xcoordinate = xCoordinate;
            loc.Ycoordinate = yCoordinate;
            loc.Name = name;
            loc.Description = description;
            loc.ImageName = ImageName;

            _locations.Add(loc);
        }

        public Location LocationAt(int xCoordinate, int ycoordinate)
        {
            foreach (Location loc in _locations)
            {
                if (loc.Xcoordinate == xCoordinate && loc.Ycoordinate == ycoordinate)
                {
                    return loc;
                }
            }

            return null;
        }
    }
}
